# README #

Storeking - master audit logs

*Sample usage*
```
var masterAuditLogs= new (require("sk-master-audit-logs"));
schema.pre("save",masterAuditLogs.auditLog);
auditOpts = {
    mergeCommentsWithStatus: true,
    nonTrackingFields: ["notification","lastUpdated", "createdBy", "modifiedBy"]
}
masterAuditLogs.init(crudder,logger,auditOpts,"CreditDebit");
```

*Options*

`nonTrackingFields` - Array of nontracking fields.

`mergeCommentsWithStatus` - Boolean, Default `false`. If true, `status` field will have message from `makerComments`.

`statusCommentKeys` - Array of field names. Default `["checkerComments", "makerComments"]`. This option is considered only if `mergeCommentsWithStatus` is true.

Module depends on swagger mongoose crud for createdBy & modifiedBy fields. Also uses cuti & lodash library internally.

Makes the following request to create audit log entry

`POST utilities/v1/auditLog`

Warning: This is not a generic module. This depends on storeking services.